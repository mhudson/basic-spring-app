package server.credits.domain;

import server.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Michael Hudson
 *
 * @since 15/05/2015.
 */
@Entity
@Table(name = "authors")
public class Author extends AbstractEntity {
    @Column(name = "name")
    private String fullName;
    @Column(name = "role")
    private String role;

    //With Hibernate you MUST have an empty constructor
    public Author() {}

    public Author(String fullName, String role) {
        this.fullName = fullName;
        this.role = role;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Author)) return false;

        Author author = (Author) o;

        if (!fullName.equals(author.fullName)) return false;
        if (!role.equals(author.role)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = fullName.hashCode();
        result = 31 * result + role.hashCode();
        return result;
    }
}
