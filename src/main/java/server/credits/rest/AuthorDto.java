package server.credits.rest;

/**
 * Created by Michael Hudson
 *
 * @since 15/05/2015.
 */
public class AuthorDto {
    private Long id;
    private String authorName;
    private String authorRole;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorRole() {
        return authorRole;
    }

    public void setAuthorRole(String authorRole) {
        this.authorRole = authorRole;
    }
}
