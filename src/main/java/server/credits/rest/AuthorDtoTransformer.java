package server.credits.rest;

import org.springframework.stereotype.Component;
import server.credits.domain.Author;
import server.util.Transform;

/**
 * Created by Michael Hudson
 *
 * @since 15/05/2015.
 */
@Component
public class AuthorDtoTransformer implements Transform<Author, AuthorDto> {
    @Override
    public AuthorDto transform(Author author) {
        AuthorDto dto = new AuthorDto();
        dto.setId(author.getId());
        dto.setAuthorName(author.getFullName());
        dto.setAuthorRole(author.getRole());
        return dto;
    }
}
