package server.credits.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import server.credits.domain.Author;
import server.credits.service.CreditsService;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michael Hudson
 * @since 14/05/2015.
 */
@RestController
@Transactional
public class CreditsController {

    private CreditsService creditsService;
    private AuthorDtoTransformer authorDtoTransformer;

    @Autowired
    public CreditsController(CreditsService creditsService, AuthorDtoTransformer authorDtoTransformer) {
        this.creditsService = creditsService;
        this.authorDtoTransformer = authorDtoTransformer;
    }

    @RequestMapping(value = "/authors", method = RequestMethod.GET)
    public List<AuthorDto> listAuthors() {
        List<AuthorDto> authorDtos = new ArrayList<AuthorDto>();
        for (Author author : creditsService.listAuthors()) {
            authorDtos.add(authorDtoTransformer.transform(author));
        }
        return authorDtos;
    }
}
