package server.credits.service;

import server.credits.domain.Author;
import java.util.List;

/**
 * Created by Michael Hudson
 * @since 14/05/2015.
 */
public interface CreditsService {
    public List<Author> listAuthors();
}
