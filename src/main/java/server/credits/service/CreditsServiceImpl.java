package server.credits.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.credits.data.CreditsDao;
import server.credits.domain.Author;

import java.util.List;

/**
 * Created by Michael Hudson
 * @since 14/05/2015.
 */
@Service
public class CreditsServiceImpl implements CreditsService {

    private CreditsDao creditsDao;

    @Autowired
    public CreditsServiceImpl(CreditsDao creditsDao) {
        this.creditsDao = creditsDao;
    }

    @Override
    public List<Author> listAuthors() {
        return creditsDao.listAll();
    }
}
