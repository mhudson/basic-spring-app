package server.credits.data;

import server.credits.domain.Author;

import java.util.List;

/**
 * Created by Michael Hudson
 *
 * @since 16/05/2015.
 */
public interface CreditsDao {
    public List<Author> listAll();
}
