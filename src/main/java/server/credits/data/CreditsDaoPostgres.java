package server.credits.data;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import server.credits.domain.Author;

import java.util.List;

/**
 * Created by Michael Hudson
 *
 * @since 16/05/2015.
 */
@Repository
public class CreditsDaoPostgres implements CreditsDao {

    private SessionFactory sessionFactory;

    @Autowired
    public CreditsDaoPostgres(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Author> listAll() {
        return sessionFactory.getCurrentSession().createCriteria(Author.class).list();
    }
}
