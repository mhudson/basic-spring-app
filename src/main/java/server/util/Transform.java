package server.util;

/**
 * Created by Michael Hudson
 *
 * @since 15/05/2015.
 */
public interface Transform<Source, Destination> {
    public Destination transform(Source source);
}
