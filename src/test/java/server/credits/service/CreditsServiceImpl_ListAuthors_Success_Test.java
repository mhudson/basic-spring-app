package server.credits.service;

import org.junit.Assert;
import org.junit.Ignore;
import org.mockito.Mockito;
import server.AbstractUnitTest;
import server.credits.data.CreditsDao;
import server.credits.domain.Author;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Michael Hudson
 *
 * @since 15/05/2015.
 */
public class CreditsServiceImpl_ListAuthors_Success_Test extends AbstractUnitTest{

    //Set up all of the test variables here. These are values that the test will not change, and later on when
    // we are making sure our action works as expected these constants will make our Assert statements easier to read.
    private static final List<Author> EXPECTED_AUTHORS = Arrays.asList(new Author("Michael Hudson", "Java Daddy"),
            new Author("Aldous Huxley", "Text Daddy"),
            new Author("Dr. Seuss", "Medical Doctor"));

    //These are working variables for the test. Anything the test can change or anything that will require mocking
    // should be declared here.
    private CreditsServiceImpl creditsService;
    private List<Author> actualAuthors;

    @Override
    public void setup() {
        CreditsDao creditsDao = Mockito.mock(CreditsDao.class);
        Mockito.when(creditsDao.listAll()).thenReturn(EXPECTED_AUTHORS);
        creditsService = new CreditsServiceImpl(null);
    }

    @Override
    public void action() {
        //Really shouldn't be testing this method since all it does is pass through its parameters,
        // this is really just to show how to set up tests.
        actualAuthors = creditsService.listAuthors();
    }

    @Override
    public void verify() {
        Assert.assertArrayEquals(EXPECTED_AUTHORS.toArray(), actualAuthors.toArray());
    }
}
