package server;

import org.junit.Test;

/**
 * Created by Michael Hudson
 *
 * @since 15/05/2015.
 */
public abstract class AbstractUnitTest {

    /**
     * Set up the test scenario.
     */
    public abstract void setup();

    /**
     * The action you are testing. This would ideally be a single line of code executing a single method call on
     * the element under test.
     */
    public abstract void action();

    /**
     * Assert that the result of the action is in line with what is expected.
     */
    public abstract void verify();

    @Test
    public void runTest() {
        setup();
        action();
        verify();
    }
}
